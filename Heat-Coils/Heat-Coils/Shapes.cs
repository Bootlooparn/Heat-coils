﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heat_Coils
{
    public class Shapes
    {
        public Shapes() { }

        public List<Point> CreateGrid(int width, int height) {
            List<Point> Grid = new List<Point>();

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Grid.Add(new Point(i, j));
                }
            }

            return Grid;
        }
    }
}
