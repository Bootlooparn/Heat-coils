﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heat_Coils
{
    public class Node
    {
        private Point nodeCoordinate;
        private Node prevNode;

        public Node(Point p) {
            nodeCoordinate = p;
        }

        public Node GetPrev() {
            return prevNode;
        }

        public void SetPrev(Node n) {
            prevNode = n;
        }

        public Point GetCoordinate() {
            return nodeCoordinate;
        }
    }
}
