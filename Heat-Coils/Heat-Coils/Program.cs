﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heat_Coils
{
    class Program
    {
        static void Main(string[] args)
        {
            

            Shapes shape = new Shapes();
            List<Point> testGrid = shape.CreateGrid(6, 6);
            //List<Point> coordinates = new List<Point> {new Point(1, 1), new Point(1, 2), new Point(1, 3), new Point(2, 1), new Point(2, 2), new Point(2, 3), new Point(3, 1), new Point(3, 2), new Point(3, 3)};

            Paths path = new Paths();

            var watch = System.Diagnostics.Stopwatch.StartNew();

            List<List<Point>> example = path.GetPaths(testGrid, new Point(1, 1), new Point(3, 1));

            watch.Stop();

            var time = watch.ElapsedMilliseconds;

            Console.WriteLine(time);

            Console.ReadLine();
        }
    }
}
