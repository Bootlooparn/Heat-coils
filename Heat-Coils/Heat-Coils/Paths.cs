﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Heat_Coils
{
    public class Paths
    {
        private static List<List<Point>> paths = new List<List<Point>>();

        public Paths() { }

        // Really?
        public List<List<Point>> GetPaths(List<Point> coordinates, Point start, Point end) {
            FindPath(coordinates, start, end);

            return paths;
        }

        // Find all the paths from point A to point B
        private void FindPath(List<Point> coordinates, Point start, Point end, Node prevN = null) {

            List<Point> newCoordinates = coordinates.ConvertAll(Point => new Point(Point.X, Point.Y));
            newCoordinates.Remove(start);

            List<Point> closestsPoints = GetNearestPoints(newCoordinates, start);

            Node n = new Node(start);

            if (start == end)
            {
                n.SetPrev(prevN);
                List<Point> path = Path(n);
                paths.Add(path);
            }
            else if (prevN != null)
            {
                n.SetPrev(prevN);
            }

            for (int i = 0; i < closestsPoints.Count; i++)
            {
                Point p = closestsPoints[i];

                FindPath(newCoordinates, p, end, n);
            }
        }

        // Find the nearest points given a reference point
        private List<Point> GetNearestPoints(List<Point> points, Point insertionPoint) {

            List<Point> closestsPoints = new List<Point>();

            for (int i = 0; i < points.Count; i++)
            {
                if (closestsPoints.Count == 4)
                {
                    return closestsPoints;
                }

                Point p = points[i];

                if ((p.X == insertionPoint.X && (p.Y == insertionPoint.Y + 1 || p.Y == insertionPoint.Y - 1)) || (p.Y == insertionPoint.Y && (p.X == insertionPoint.X + 1 || p.X == insertionPoint.X - 1)))
                {
                    closestsPoints.Add(points[i]);
                }
            }
            return closestsPoints;
        }

        // Find the paths from he last node to the first node
        private List<Point> Path(Node n, List<Point> pathList = null) {

            if (pathList == null)
            {
                pathList = new List<Point>();
            }

            pathList.Add(n.GetCoordinate());

            if (n.GetPrev() != null)
            {
                pathList = Path(n.GetPrev(), pathList);
                return pathList;
            }
            else
            {
                return pathList;
            }
        }

        // Creates a new list through deep copy otherwise shallow copy problems will occur
        private List<Point> CopyPaths(List<Point> pathList)
        {
            List<Point> copiedPaths = pathList.ConvertAll(Point => new Point(Point.X, Point.Y));

            return copiedPaths;
        }
    }
}
